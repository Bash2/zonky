import React from 'react';
import thunk from 'redux-thunk'
import 'typeface-roboto';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from 'material-ui/styles';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter,  Route, browserHistory } from 'react-router-dom'

import rootReducer from './reducers';
import { getLoans } from './actions';
import HomeView from './views/HomeView';
import LoanView from './views/LoanView';

let store = createStore(rootReducer, applyMiddleware(thunk));
store.dispatch(getLoans());

const App = ({classes}) => (
	<MuiThemeProvider>
		<Provider store={store}>
			<BrowserRouter history={browserHistory}>
				<div>
					<Route exact path="/" component={HomeView} />
					<Route path="/loan/:id" component={LoanView} />
				</div>
			</BrowserRouter>
		</Provider>
	</MuiThemeProvider>
);

export default App