const initialState = {
	lastUpdated: '',
	sortOrder: 'DESC',
	sortField: 'rating',
	loans: [],
	showErrorDialog: false
}

const zonky = (state = initialState, action) => {
	switch(action.type){
		case 'CHANGE_SORT_ORDER':
			return Object.assign({}, state, {sortOrder: action.sortOrder});
		case 'CHANGE_SORT_FIELD':
			return Object.assign({}, state, {sortField: action.sortField});
		case 'GET_LOANS':
			if(action.data){
				return Object.assign({}, state, {loans: action.data});
			} else if(action.error) {
				return Object.assign({}, state, {showErrorDialog: true});
			}
			break;
		default:
			return state;
	}
}

export default zonky