import 'whatwg-fetch';

const getLoansSuccess = (data) => {
	return {
		type: 'GET_LOANS',
		status: 'success',
		data
	}
}

const getLoansError = (error) => {
	return {
		type: 'GET_LOANS',
		status: 'error',
		error
	}
}

export const getLoans = () => {
	return function(dispatch) {
		//Use Yahoo YQL to bypass SOP (Zonky API doesn't allow CORS)
		var query = 'select * from json where url="https://api.zonky.cz/loans/marketplace"';
		return fetch(`https://query.yahooapis.com/v1/public/yql?q=${encodeURI(query)}&format=json`)
			.then(response => response.json())
			.then(data => {
				let results = data.query.results.json.json;
				dispatch(getLoansSuccess(results));
			})
			.catch(error => {
				dispatch(getLoansError(error));
			});
	}
}

export const changeSortOrder = (order) => {
	return {
		type: 'CHANGE_SORT_ORDER',
		sortOrder: order
	}
}

export const changeSortField = (field) => {
	return {
		type: 'CHANGE_SORT_FIELD',
		sortField: field
	}
}

