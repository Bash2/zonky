import React from 'react';
import configureStore from 'redux-mock-store'
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { MuiThemeProvider } from 'material-ui/styles';

import SortedLoanCardList from './SortedLoanCardList';

describe('Testing sort function in SortedLoanCardList', ()=> {
	//Integer values are string due to Yahoo YQL converting int values to string
	const initialState = {
		loans: [
			{
				"id": "1",
				"name": "Test 1",
				"story": "This is testing Loan 1",
				"photos": {
					"url": "test"
				},
	      		"termInMonths": "84",
	      		"rating": "B",
	      		"amount": "140000.0",
	      		"deadline": "2017-05-25T15:55:05.672+02:00",
	      	},
			{
				"id": "2",
				"name": "Test 2",
				"story": "This is testing Loan 2",
				"photos": {
					"url": "test"
				},
	            "termInMonths": "66",
	            "rating": "AAAA",
	            "amount": "170000.0",
	            "deadline": "2017-05-25T12:37:38.281+02:00",
	      	},
	      	{
	      		"id": "3",
	      		"name": "Test 3",
				"story": "This is testing Loan 3",
				"photos": {
					"url": "test"
				},
	      		"termInMonths": "47",
	      		"rating": "D",
	      		"amount": "100000.0",
	      		"deadline": "2017-05-25T17:00:17.862+02:00",
	      	},
	      	{
	      		"id": "4",
	      		"name": "Test 4",
				"story": "This is testing Loan 4",
				"photos": {
					"url": "test"
				},
	      		"termInMonths": "78",
	      		"rating": "AAA",
	      		"amount": "150000.0",
	      		"deadline": "2017-05-25T14:05:22.590+02:00",
	      	},
	      	{
	      		"id": "5",
	      		"name": "Test 5",
				"story": "This is testing Loan 5",
				"photos": {
					"url": "test"
				},
	      		"termInMonths": "60",
	      		"rating": "AA",
	      		"amount": "250000.0",
	      		"deadline": "2017-05-25T15:52:15.635+02:00",
	      	}	
		]
	}

	const wrapper = (mod) => {
		const store = configureStore()(Object.assign(initialState, mod));
		return shallow(
			<SortedLoanCardList store={store} />
		)
	}

	const matchLoanIDs = (loans, ids) => {
		for(var i = 0; i < loans.length; i++){
			expect(loans[i].id).toMatch(ids[i]);
		}
	}

	test('Sort by duration in ascending order', () => {
		const expected = ["3", "5", "2", "4", "1"];
		const state = {
			sortOrder: 'ASC',
			sortField: 'duration'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by duration in descending order', () => {
		const expected = ["1", "4", "2", "5", "3"];
		const state = {
			sortOrder: 'DESC',
			sortField: 'duration'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by rating in ascending order', () => {
		const expected = ["3", "1", "5", "4", "2"];
		const state = {
			sortOrder: 'ASC',
			sortField: 'rating'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by rating in descending order', () => {
		const expected = ["2", "4", "5", "1", "3"];
		const state = {
			sortOrder: 'DESC',
			sortField: 'rating'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by amount in ascending order', () => {
		const expected = ["3", "1", "4", "2", "5"];
		const state = {
			sortOrder: 'ASC',
			sortField: 'amount'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by amount in descending order', () => {
		const expected = ["5", "2", "4", "1", "3"];
		const state = {
			sortOrder: 'DESC',
			sortField: 'amount'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by deadline in ascending order', () => {
		const expected = ["2", "4", "5", "1", "3"];
		const state = {
			sortOrder: 'ASC',
			sortField: 'deadline'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})

	test('Sort by deadline in descending order', () => {
		const expected = ["3", "1", "5", "4", "2"];
		const state = {
			sortOrder: 'DESC',
			sortField: 'deadline'
		}
		const actual = wrapper(state).props().loans;
		matchLoanIDs(actual, expected);
	})
})