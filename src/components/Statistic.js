import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';

const Statistic = ({value, label}) => (
	<Grid item>
		<Typography type="headline" style={{textAlign: 'center'}}>{value}</Typography>
		<Typography style={{textAlign: 'center', fontWeight: 300}}>{label}</Typography>
	</Grid>
)

Statistic.propTypes = {
	value: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired
}

export default Statistic