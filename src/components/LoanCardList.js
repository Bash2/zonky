import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import LoanCard from './LoanCard';

const LoanCardList = ({loans}) => (
	<Grid container gutter={16}>
		{
			loans.map(loan => (
				<LoanCard key={loan.id} loan={loan} />
			))
		}	
	</Grid>
)

LoanCardList.propTypes = {
	loans: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		photos: PropTypes.shape({
			url: PropTypes.string.isRequired
		}),
		name: PropTypes.string.isRequired,
		story: PropTypes.string.isRequired
	}).isRequired).isRequired
}

export default LoanCardList