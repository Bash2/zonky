import React from 'react';
import renderer from 'react-test-renderer';
import { MuiThemeProvider } from 'material-ui/styles';
import Statistic from './Statistic';


test('Statistic snapshot test', () => {
	const component = renderer.create(
		<MuiThemeProvider>
			<Statistic value="42" label="The Answer" />
		</MuiThemeProvider>
	)

	let snap = component.toJSON();
	expect(snap).toMatchSnapshot();
})