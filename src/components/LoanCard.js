import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import { Link } from 'react-router-dom'
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import { withStyles, createStyleSheet } from 'material-ui/styles';

const styleSheet = createStyleSheet('LoanCard', () => ({
  image: {
  	height: 200, 
  	backgroundPosition: 'center', 
  	backgroundRepeat: 'no-repeat', 
  	backgroundSize: 'cover'
  },
  link: {
  	textDecoration: 'none'
  }
}));

const trimStory = (story) => (
	story.split(" ").slice(0, 20).join(" ")
)

const LoanCard = ({classes, loan}) => (
	<Grid item xs={3}>
		<Link to={`/loan/${loan.id}`} className={classes.link}>
			<Card>
				<CardMedia>
				  <div className={classes.image} style={{backgroundImage: `url('https://api.zonky.cz${loan.photos.url}')`}}></div>
				</CardMedia>
				<CardContent>
				  <Typography type="headline" component="h2">{loan.name}</Typography>
				  <Typography component="p">{trimStory(loan.story)}</Typography>
				</CardContent>
			</Card>
		</Link>
	</Grid>
)

LoanCard.propTypes = {
	loan: PropTypes.shape({
		id: PropTypes.string.isRequired,
		photos: PropTypes.shape({
			url: PropTypes.string.isRequired
		}),
		name: PropTypes.string.isRequired,
		story: PropTypes.string.isRequired
	})
}

export default withStyles(styleSheet)(LoanCard)