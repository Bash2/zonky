import { connect } from 'react-redux';
import LoanCardList from './LoanCardList';

//Map to assign a numerical value to rating to allow sorting
const ratingMap = new Map();
ratingMap.set('AAAAA', 8);
ratingMap.set('AAAA', 7);
ratingMap.set('AAA', 6);
ratingMap.set('AA', 5);
ratingMap.set('A', 4);
ratingMap.set('B', 3);
ratingMap.set('C', 2);
ratingMap.set('D', 1);

const assignRatingValues = (loans) => {
	loans.forEach((loan) => {
		loan.ratingValue = ratingMap.get(loan.rating);
	})
}

const sortLoansByProperty = (loans, property, order) => (
	loans.slice(0).sort((a, b) => {
		if(a[property] < b[property]){
			return order === 'ASC' ? -1 : 1
		} else {
			return order === 'ASC' ? 1 : -1
		}
	})
)

const sortLoans = (loans, sortField, sortOrder) => {
	switch(sortField){
		case 'duration':
			return sortLoansByProperty(loans, 'termInMonths', sortOrder);
		case 'rating':
			assignRatingValues(loans);
			return sortLoansByProperty(loans, 'ratingValue', sortOrder);
		case 'amount':
			return sortLoansByProperty(loans, 'amount', sortOrder);
		case 'deadline':
			return sortLoansByProperty(loans, 'deadline', sortOrder);
		default:
			return sortLoansByProperty(loans, 'ratingValue', 'DESC');
	}
}

const mapStateToProps = (state) => ({
	loans: sortLoans(state.loans, state.sortField, state.sortOrder)
})

const SortedLoanCardList = connect(mapStateToProps)(LoanCardList);

export default SortedLoanCardList
