import React from 'react';
import renderer from 'react-test-renderer';
import { MuiThemeProvider } from 'material-ui/styles';
import { BrowserRouter } from 'react-router-dom'
import LoanCard from './LoanCard';


const loan = {
    "id": "84218",
    "name": "Refinancování půjček doplatek",
    "story": "peníze budou použity na doplacení půjčky s vyšším úrokem, a z ušetřených úroků se bude postupně opravovat domeček na stáří.",
    "photos": {
      "url": "/loans/84218/photos/9339"
    }
}

test('LoanCard snapshot test', () => {
	const component = renderer.create(
		<MuiThemeProvider>
			<BrowserRouter>
				<LoanCard loan={loan} />
			</BrowserRouter>
		</MuiThemeProvider>
	)

	let snap = component.toJSON();
	expect(snap).toMatchSnapshot();
})