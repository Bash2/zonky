import React from 'react';
import renderer from 'react-test-renderer';
import { MuiThemeProvider } from 'material-ui/styles';
import { BrowserRouter, Route } from 'react-router-dom'
import { View as LoanView } from './LoanView';


const loan =  {
	"id": "84218",
	"url": "https://app.zonky.cz/loan/84218",
	"name": "Refinancování půjček doplatek",
	"story": "peníze budou použity na doplacení půjčky s vyšším úrokem, a z ušetřených úroků se bude postupně opravovat domeček na stáří.",
	"purpose": "6",
	"photos": {
	 	"name": "6",
	 	"url": "/loans/84218/photos/9339"
	},
	"userId": "108041",
	"nickName": "zonky108041",
	"termInMonths": "66",
	"interestRate": "0.0499",
	"rating": "AAAA",
	"topped": "null",
	"amount": "170000.0",
	"remainingInvestment": "153200.0",
	"investmentRate": "0.0988235294117647",
	"covered": "false",
	"datePublished": "2017-05-23T12:47:05.015+02:00",
	"published": "true",
	"deadline": "2017-05-25T12:37:38.281+02:00",
	"investmentsCount": "35",
	"questionsCount": "0",
	"region": "8",
	"mainIncomeType": "EMPLOYMENT"
}

test('LoanView snapshot test', () => {
	const component = renderer.create(
		<MuiThemeProvider>
			<BrowserRouter>
				<Route path="/" component={LoanView} />
			</BrowserRouter>
		</MuiThemeProvider>
	)

	let snap = component.toJSON();
	expect(snap).toMatchSnapshot();
})