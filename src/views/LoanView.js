import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import BackIcon from 'material-ui-icons/ArrowBack';
import { LinearProgress } from 'material-ui/Progress';
import { connect } from 'react-redux';
import { withStyles, createStyleSheet } from 'material-ui/styles';

import Statistic from '../components/Statistic';

const styleSheet = createStyleSheet('LoanView', () => ({
  content:{
  	marginTop: 80,
  	padding: 20
  },
  bold: {
  	fontWeight: 'bold'
  },
  story: {
  	marginTop: 20,
  	textAlign: 'justify'
  },
  progressText: {
  	display: 'inline'
  }
}));

const formatDate = (date) => {
	date = new Date(date);
	return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
}

const Component = ({classes, loan, history}) => (
	<div>
		{ loan &&
		<div>
			<AppBar className={classes.appBar}>
				<Toolbar>
					<IconButton contrast onClick={history.goBack}>
						<BackIcon />
					</IconButton>
					<Typography type="title" colorInherit>{loan.name}</Typography>
				</Toolbar>
			 </AppBar>
		 	<Grid container className={classes.content}>
		 		<Grid item>
			 		<Grid container direction="column">
			 			<Grid item>
			 				<img src={`https://api.zonky.cz${loan.photos.url}`} alt="Loan description"/>
			 			</Grid>
			 			<Grid item>
			 				<Typography type="subheading">Progress</Typography>
			 				<LinearProgress mode="determinate" value={(loan.remainingInvestment/loan.amount) * 100} />
			 				<div>
			 					<Typography style={{float: 'left'}} className={classes.progressText}>Remaining: {Math.trunc(loan.remainingInvestment)}Kč</Typography>
			 					<Typography style={{float: 'right'}} className={classes.progressText}>Total: {Math.trunc(loan.amount)}Kč</Typography>
			 				</div>
			 			</Grid>
			 			<Grid item>
				 			<Grid container className={classes.statistics} justify="center">
				 				<Statistic label="Rating" value={loan.rating} />
				 				<Statistic label="Rate" value={`${loan.interestRate * 100}%`} />
				 				<Statistic label="Duration" value={loan.termInMonths} />
				 				<Statistic label="Investors" value={loan.investmentsCount} />
				 			</Grid>
			 			</Grid>
			 		</Grid>
		 		</Grid>
		 		<Grid item xs={7}>
		 			<Typography type="headline">{loan.name}</Typography>
		 			<Typography type="subheading" className={classes.bold}>{loan.nickName}</Typography>
		 			<Typography className={classes.story}>{loan.story}</Typography>
		 			<Typography type="subheading" style={{marginTop: 5}}  className={classes.bold}>Deadline: {formatDate(loan.deadline)}</Typography>
		 		</Grid>
		 	</Grid>
		</div>
		}
	</div>
);

Component.propTypes = {
	loan: PropTypes.shape({
		id: PropTypes.string.isRequired,
		photos: PropTypes.shape({
			url: PropTypes.string.isRequired
		}).isRequired,
		name: PropTypes.string.isRequired,
		nickName: PropTypes.string.isRequired,
		deadline: PropTypes.string.isRequired,
		story: PropTypes.string.isRequired,
		remainingInvestment: PropTypes.string.isRequired,
		amount: PropTypes.string.isRequired,
		rating: PropTypes.string.isRequired,
		interestRate: PropTypes.string.isRequired,
		termInMonths: PropTypes.string.isRequired,
		investmentsCount: PropTypes.string.isRequired
	})
}

const findLoanById = (loans, id) => (
	loans.find((loan)=>(loan.id === id))
)

const mapStateToProps = (state, ownProps) => ({
	loan: state.loans ? findLoanById(state.loans, ownProps.match.params.id) : null
})
export const View = withStyles(styleSheet)(Component)
export default connect(mapStateToProps)(View);