import React from 'react';
import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import { connect } from 'react-redux';
import { FormLabel } from 'material-ui/Form';
import { LabelRadio, RadioGroup } from 'material-ui/Radio';
import { withStyles, createStyleSheet } from 'material-ui/styles';

import SortedLoanCardList from '../components/SortedLoanCardList';
import {changeSortOrder, changeSortField} from '../actions';

const styleSheet = createStyleSheet('HomeView', () => ({
  content:{
  	marginTop: 100,
  	padding: 20
  },
  appBar:{
  	height: 70,
  	paddingTop: 10
  },
  radiogroup: {
  	height: 30
  },
  white: {
  	color: 'white'
  }
}));

const HomeView = ({classes, sortOrder, changeSortOrder, sortField, changeSortField}) => (
	<div>
		 <AppBar className={classes.appBar}>
			<Toolbar>
				<Typography type="title" colorInherit>Zonky</Typography>
				<Grid container>
					<Grid item xs={1}>
					</Grid>
			  		<Grid item>
			  			<FormLabel className={classes.white}>Sort by</FormLabel>
			  			<RadioGroup className={classes.radiogroup} selectedValue={sortField} onChange={changeSortField} row>
			  				<LabelRadio className={classes.white} label="Rating" value="rating" />
			  				<LabelRadio className={classes.white} label="Duration" value="duration"/>
			  				<LabelRadio className={classes.white} label="Amount" value="amount" />
			  				<LabelRadio className={classes.white} label="Deadline" value="deadline" />
			  			</RadioGroup>
			  		</Grid>
			  		<Grid item>
		  				<FormLabel className={classes.white}>Order</FormLabel>
			  			<RadioGroup className={classes.radiogroup} selectedValue={sortOrder} onChange={changeSortOrder} row>
			  				<LabelRadio className={classes.white}  label="Ascending" value="ASC" />
			  				<LabelRadio className={classes.white}  label="Descending" value="DESC"/>
			  			</RadioGroup>
		  			</Grid>
		  		</Grid>
			</Toolbar>
		  </AppBar>
		  <div className={classes.content}>
		  	<SortedLoanCardList />
		  </div>
	</div>
);

const mapStateToProps = (state) => ({
	sortOrder: state.sortOrder,
	sortField: state.sortField
})

const mapDispatchToProps = (dispatch) => ({
	changeSortOrder: (event, value) => {
		dispatch(changeSortOrder(value));
	},
	changeSortField: (event, value) => {
		dispatch(changeSortField(value));
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styleSheet)(HomeView));